Jinja2
MarkupSafe
PyYAML
distro
msgpack>=0.5,!=0.5.5,<1.0.0
pycryptodomex>=3.9.7
pyzmq>=17.0.0
requests>=1.0.0
